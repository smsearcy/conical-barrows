#
# This file is autogenerated by pip-compile with python 3.10
# To update, run:
#
#    pip-compile dev-requirements.in
#
alabaster==0.7.12
    # via sphinx
attrs==21.4.0
    # via pytest
babel==2.9.1
    # via sphinx
beautifulsoup4==4.10.0
    # via webtest
certifi==2021.10.8
    # via requests
charset-normalizer==2.0.10
    # via requests
coverage[toml]==6.2
    # via pytest-cov
docutils==0.17.1
    # via sphinx
idna==3.3
    # via requests
imagesize==1.3.0
    # via sphinx
iniconfig==1.1.1
    # via pytest
jinja2==3.0.3
    # via sphinx
markupsafe==2.0.1
    # via jinja2
packaging==21.3
    # via
    #   pytest
    #   sphinx
pluggy==1.0.0
    # via pytest
py==1.11.0
    # via pytest
pygments==2.11.2
    # via sphinx
pyparsing==3.0.6
    # via packaging
pytest==6.2.5
    # via
    #   -r dev-requirements.in
    #   pytest-cov
pytest-cov==3.0.0
    # via -r dev-requirements.in
pytz==2021.3
    # via babel
requests==2.27.1
    # via sphinx
snowballstemmer==2.2.0
    # via sphinx
soupsieve==2.3.1
    # via beautifulsoup4
sphinx==4.4.0
    # via -r dev-requirements.in
sphinxcontrib-applehelp==1.0.2
    # via sphinx
sphinxcontrib-devhelp==1.0.2
    # via sphinx
sphinxcontrib-htmlhelp==2.0.0
    # via sphinx
sphinxcontrib-jsmath==1.0.1
    # via sphinx
sphinxcontrib-qthelp==1.0.3
    # via sphinx
sphinxcontrib-serializinghtml==1.1.5
    # via sphinx
toml==0.10.2
    # via pytest
tomli==2.0.0
    # via coverage
urllib3==1.26.8
    # via requests
waitress==2.0.0
    # via webtest
webob==1.8.7
    # via webtest
webtest==3.0.0
    # via -r dev-requirements.in
